﻿//*********************************************************************************************************************
// $Id: $                                                                       
//                                                                              
// Authors:                                                                     
// Jamie Currie                                                            
//*********************************************************************************************************************

using System;
using System.Threading;

//*********************************************************************************************************************
namespace xist_udp_tool {
    public class Network {

        #region Fields                                                                  
        // Static ---------------------------------------------------------------------------------------------------------
        public static string sendIP;
        public static int listenPort;
        public static int sendPort;
        public static string macAddress = "00-00-00-00-00-00";

        public static string deviceTypeId;
        public static string myId = "";
        public static bool isRunning;

        // Const ----------------------------------------------------------------------------------------------------------
        public static string serverAddress;

        // Public ---------------------------------------------------------------------------------------------------------

        // Private --------------------------------------------------------------------------------------------------------
        private static bool isAuthed = false;
        #endregion

        #region Public
        public static void UDPThread() {
            isRunning = true;
            sendIP = serverAddress;
            SpawnListeners();
            while (isRunning) {
                if (string.IsNullOrEmpty(myId)) {
                    QueryID();
                } else if (!isAuthed) {
                    SendAuthRequest();
                } else {
                    SendHeartBeat();
                }
                Thread.Sleep(1000);
            }
        }

        public static void SendUdpMessage(string msg) {
            UDPHelper.SendMessageToAddress(msg, sendIP, sendPort);
        }
        #endregion

        #region Private
        private static void SpawnListeners() {
            UDPHelper.SpawnNewListener(null, listenPort, ProcessMatchMakerCommand);
            UDPHelper.SpawnNewListener(null, Port.commandClientPort, ProcessGameManagerCommand);
        }

        private static void QueryID() {
            UDPHelper.SendMessageToAddress("QUERYID|" + deviceTypeId + "|" + macAddress, serverAddress, sendPort);
        }

        private static void SendAuthRequest() {
            string authhash = UDPHelper.BuildAuthString(myId);
            UDPHelper.SendMessageToAddress("AUTH|" + myId + "|" + deviceTypeId + "|" + authhash + "|" + Environment.MachineName, serverAddress, sendPort);
        }

        private static void SendHeartBeat() {

            string type = "";
            switch(Program.deviceType) {
                case DeviceType.LAUNCHER:
                case DeviceType.ANDROID_LAUNCHER:
                    type = "AL";
                    break;

                case DeviceType.CLIENT:
                    type = "CL";
                    break;

                case DeviceType.HAND:
                case DeviceType.SLAM:
                    type = "BP";
                    break;
            }
            UDPHelper.SendMessageToAddress("HELLO|" + type, serverAddress, sendPort);
        }

        private static void SendGamemanagerAuth() {
            UDPHelper.SendMessageToAddress("AUTH", serverAddress, Port.commandServerPort);
        }
        #endregion

        #region Threads
        private static void ProcessMatchMakerCommand(string received_data) {

            string[] _comps = received_data.Split('|');
            string _cmd = _comps[0];

            switch (_cmd) {

                case "DEVICEID":
                    myId = _comps[1];
                    break;

                case "AUTH":
                    string authStatus = _comps[1];
                    switch(authStatus) {
                        case "OK":
                            isAuthed = true;
                            if (Program.deviceType == DeviceType.CLIENT) {
                                SendGamemanagerAuth();
                            }
                            break;
                        case "FAILED":
                            isAuthed = false;
                            myId = null;
                            break;
                        default:
                            isAuthed = false;
                            break;
                    }
                    break;

                case "HELLO":
                    string status = _comps[1];
                    if (status == "FAILED") {
                        isAuthed = false;
                        myId = null;
                        QueryID();
                    }
                    break;

                case "CHANGEIP":
                    sendIP = _comps[1].Split(':')[0];
                    break;

                case "UNPAIR":
                    isAuthed = false;
                    sendIP = serverAddress;
                    break;

                case "SHUTDOWN":
                case "REBOOT":
                    Program.OnExit();
                    Thread.Sleep(1000);
                    Environment.Exit(0);
                    break;

                case "REAUTH":
                    isAuthed = false;
                    myId = null;
                    break;
            }
        }

        private static void ProcessGameManagerCommand(string received_data) {
            // maybe do something here one day
        }
        #endregion
    }
}
