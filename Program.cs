﻿using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace xist_udp_tool {
    class Program {
        public static bool printUDP = true;
        public static DeviceType deviceType;

        static void Main(string[] args) {
            try {
                handler = new ConsoleEventDelegate(ConsoleEventCallbackWin);
                SetConsoleCtrlHandler(handler, true);
            } catch (Exception e) {
                AppDomain.CurrentDomain.ProcessExit += new EventHandler(ConsoleEventCallback);
                Console.CancelKeyPress += new ConsoleCancelEventHandler(ConsoleEventCallback);
            }
            Console.WriteLine("Use 'p' to pause the feed, once paused any text will be send over udp.\n");

            Console.WriteLine("Please enter a valid IP address (Leave blank to use this machines IP)");
            string userInput = Console.ReadLine();

            if (!string.IsNullOrEmpty(userInput)) {
                Network.serverAddress = userInput;
            } else {
                Network.serverAddress = UDPHelper.getMyIp();
            }
            Console.WriteLine($"IP Address: {Network.serverAddress}\n");

            while (true) {
                Console.WriteLine("Please enter device type ( c = client, h = hand, s = slam, l = launcher, al = android_launcher )");
                userInput = Console.ReadLine().ToUpper();

                bool deviceTypeSet = false;
                switch(userInput) {
                    case "C":
                        deviceType = DeviceType.CLIENT;
                        deviceTypeSet = true;
                        break;
                    case "H":
                        deviceType = DeviceType.HAND;
                        deviceTypeSet = true;
                        break;
                    case "S":
                        deviceType = DeviceType.SLAM;
                        deviceTypeSet = true;
                        break;
                    case "L":
                        deviceType = DeviceType.LAUNCHER;
                        deviceTypeSet = true;
                        break;
                    case "AL":
                        deviceType = DeviceType.ANDROID_LAUNCHER;
                        deviceTypeSet = true;
                        break;
                }

                if (deviceTypeSet) {
                    break;
                }
            }
            Console.WriteLine($"Device type: {deviceType}\n");

            while (true) {
                Console.WriteLine("Would you like to use custom mac address? Y/N");
                userInput = Console.ReadLine().ToUpper();

                if (userInput.Equals("Y")) {
                    Console.WriteLine("Please enter a valid mac address (Leave blank for '00-00-00-00-00-00')");
                    userInput = Console.ReadLine().ToUpper();

                    if (!string.IsNullOrEmpty(userInput)) {
                        Network.macAddress = userInput;
                    }
                    break;
                } else if (userInput.Equals("N")) {
                    Network.macAddress = UDPHelper.GetMac();
                    break;
                }
            }
            Console.WriteLine($"Mac address: {Network.macAddress}\n");

            switch (deviceType) {
                case DeviceType.CLIENT:
                    Network.listenPort = Port.UDPUnityClientsPort;
                    Network.sendPort = Port.UDPGenericSendPort;
                    Network.deviceTypeId = "1";
                    break;
                case DeviceType.LAUNCHER:
                    Network.listenPort = Port.UDPAppLauncherPort;
                    Network.sendPort = Port.UDPAppLauncherSendPort;
                    Network.deviceTypeId = "x";
                    break;
                case DeviceType.ANDROID_LAUNCHER:
                    Network.listenPort = Port.UDPAppLauncherPort;
                    Network.sendPort = Port.UDPAppLauncherSendPort;
                    Network.deviceTypeId = "5";
                    break;
                case DeviceType.HAND:
                    Network.listenPort = Port.UDPHandTrackerPort;
                    Network.sendPort = Port.UDPHandTrackerSendPort;
                    Network.deviceTypeId = "3";
                    break;
                case DeviceType.SLAM:
                    Network.listenPort = Port.UDPSLAMSensorPort;
                    Network.sendPort = Port.UDPSLAMSensorSendPort;
                    Network.deviceTypeId = "4";
                    break;
            }

            Thread networkThread = new Thread(Network.UDPThread); // Start network threads
            networkThread.Start();
            while (true) {
                userInput = Console.ReadLine();
                if (userInput.ToLower().Equals("p")) {
                    printUDP = !printUDP;
                } else {
                    if (!printUDP) {
                        Network.SendUdpMessage(userInput);
                        printUDP = true;
                    }
                }
            }
        }

        public static void OnExit() {
            if (Network.isRunning) {
                Network.isRunning = false;
                UDPHelper.SendMessageToAddress("DISCONNECT|" + Network.deviceTypeId, Network.serverAddress, Network.sendPort);
                UDPHelper.CloseAllConnections();
                Thread.Sleep(1000);
            }
        }

        #region Helpers
        static ConsoleEventDelegate handler;
        delegate bool ConsoleEventDelegate(int eventType);
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);

        static bool ConsoleEventCallbackWin(int eventType) {
            if (eventType == 2) {
                OnExit();
            }
            return false;
        }

        static void ConsoleEventCallback(object sender, EventArgs e) {
            OnExit();
        }
        #endregion
    }
}
