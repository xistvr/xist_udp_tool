﻿//*********************************************************************************************************************
// $Id: $                                                                       
//                                                                              
// Authors:                                                                     
// James Burrows                                                               
//*********************************************************************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using xist_udp_tool;

//*********************************************************************************************************************
public delegate void UDPDelegate( string message );

public class UDPHelper {

    #region ScriptParameters                                                        
    #endregion

    #region Fields                 
    // Static ---------------------------------------------------------------------------------------------------------
    private static Socket sending_socket;
    private static string myIPAddress;
    private static Dictionary<int, UDPHelper> listeners = new Dictionary<int, UDPHelper>();


    // Const ----------------------------------------------------------------------------------------------------------

    // Public ---------------------------------------------------------------------------------------------------------
    static public Dictionary<string, string> encodeLookupTable = new Dictionary<string, string>();
    static public Dictionary<string, string> decodeLookupTable = new Dictionary<string, string>();

    // Private --------------------------------------------------------------------------------------------------------
    private Thread UDPClientThread;
    private int listenPort;
    private bool isMulticast;
    private string multicastIPGroup;
    private IPEndPoint localEp;
    private UdpClient UDPlistener;
    private List<KeyValuePair<string, UDPDelegate>> parentDelegates = new List<KeyValuePair<string, UDPDelegate>>();

    #endregion

    #region Methods
    public void Start() {
        getMyIp();
        try {
            if (isMulticast) {
                UDPlistener = new UdpClient();
                UDPlistener.Client.ReceiveBufferSize = 16384;
                localEp = new IPEndPoint(IPAddress.Any, listenPort);
                UDPlistener.Client.Bind(localEp);

                IPAddress multicastaddress = IPAddress.Parse(multicastIPGroup);
                UDPlistener.JoinMulticastGroup(multicastaddress);
            } else {
                UDPlistener = new UdpClient(listenPort);
                UDPlistener.Client.ReceiveBufferSize = 16384;
            }

            UDPClientThread = new Thread(UDPCode);
            UDPClientThread.Start();
        } catch (Exception e) {
            Console.WriteLine("Unable to start UDP listener on port: " + listenPort + "");
            Console.WriteLine(e.Message);
        }
    }

    public void End() {
        Console.WriteLine("Attempting to kill UDP thread for port " + listenPort);
        UDPHelper.SendMessageToAddress("KILLCONN", myIPAddress, listenPort);
        listeners.Remove(listenPort);
    }

    #endregion

    #region Implementation
    #endregion

    #region Interfaces
    #endregion

    #region Threads
    private void UDPCode() {
        bool UDPrunning = true;
        Console.WriteLine("Setting up UDP listener");
        IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
        string received_data;
        byte[] receive_byte_array;

        while (UDPrunning) {
            try {
                if (isMulticast) {
                    receive_byte_array = UDPlistener.Receive(ref localEp); //<-- this blocks with no clean way to exit
                    received_data = Encoding.UTF8.GetString(receive_byte_array);
                    received_data = transcodeCommand(received_data, false);
                    List<KeyValuePair<string, UDPDelegate>> tempDelegates = parentDelegates.ToList();
                    foreach (KeyValuePair<string, UDPDelegate> delegatepair in tempDelegates) {
                        if (localEp.Address.ToString() == delegatepair.Key || delegatepair.Key == null) {
                            delegatepair.Value(received_data);
                        }
                    }
                } else {
                    receive_byte_array = UDPlistener.Receive(ref RemoteIpEndPoint); //<-- this blocks with no clean way to exit
                    received_data = Encoding.UTF8.GetString(receive_byte_array);
                    if (string.IsNullOrEmpty(received_data)) {
                        Console.WriteLine("UDP recieved empty messge: " + Environment.StackTrace);
                    }

                    received_data = transcodeCommand(received_data, false);
                    if (Program.printUDP) {
                        Console.WriteLine("<- " + RemoteIpEndPoint.Address + " - " + received_data);
                    }

                    if (RemoteIpEndPoint.Address.ToString() == myIPAddress) {
                        if (received_data.Contains("KILLCONN")) {
                            Console.WriteLine("Recieved kill command from local inferface network address, killing lister for port " + listenPort);
                            UDPrunning = false; //exit thread cleanly
                            UDPlistener.Close();
                            break;
                        } else {
                            List<KeyValuePair<string, UDPDelegate>> tempDelegates = parentDelegates.ToList();
                            foreach (KeyValuePair<string, UDPDelegate> delegatepair in tempDelegates) {
                                if (RemoteIpEndPoint.Address.ToString() == delegatepair.Key || delegatepair.Key == null) {
                                    delegatepair.Value(received_data);
                                }
                            }
                        }
                    } else {
                        List<KeyValuePair<string, UDPDelegate>> tempDelegates = parentDelegates.ToList();
                        foreach (KeyValuePair<string, UDPDelegate> delegatepair in tempDelegates) {
                            if (RemoteIpEndPoint.Address.ToString() == delegatepair.Key || delegatepair.Key == null) {
                                delegatepair.Value(received_data);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
        }
    }

    public static string transcodeCommand(string input, bool encode) {
        if (encodeLookupTable.Count == 0 || decodeLookupTable.Count == 0) {
            return input;
        }
        string output = "";
        string[] parts = input.Split('|');
        int count = 0;
        foreach (string part in parts) {
            if (encode) {
                output += (encodeLookupTable.ContainsKey(part)) ? encodeLookupTable[part] : part;
            } else {
                output += (decodeLookupTable.ContainsKey(part)) ? decodeLookupTable[part] : part;
            }
            if (count < parts.Length - 1) {
                output += "|";
            }
            count++;
        }
        return output;
    }

    public static bool SendMessageToAddress(string message, string ipAddress, int port) {
        if (Program.printUDP) {
            Console.WriteLine("-> " + ipAddress + ":" + port + " - " + message);
        }
        if (string.IsNullOrEmpty(message)) {
            Console.WriteLine("UDP sent empty messge: " + Environment.StackTrace);
        }

        message = transcodeCommand(message, true);
        if (UDPHelper.sending_socket == null) {
            UDPHelper.sending_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        }
        bool ok = false;
        IPAddress send_to_address = IPAddress.Parse(ipAddress);
        IPEndPoint sending_end_point = new IPEndPoint(send_to_address, port); //we send on the headsets listen port
        byte[] send_buffer = Encoding.UTF8.GetBytes(message);
        try {
            sending_socket.SendTo(send_buffer, sending_end_point);
            ok = true;
        } catch (Exception send_exception) {
            Console.WriteLine(" Exception " + send_exception.Message);
        }
        return ok;
    }

    public static bool SendMulticastMessage(string message, string ipaddress, int port) {
        message = transcodeCommand(message, true);
        bool ok = false;
        UdpClient udpclient = new UdpClient();

        IPAddress multicastaddress = IPAddress.Parse(ipaddress);
        udpclient.JoinMulticastGroup(multicastaddress);
        IPEndPoint remoteep = new IPEndPoint(multicastaddress, port);
        byte[] send_buffer = Encoding.UTF8.GetBytes(message);
        try {
            udpclient.Send(send_buffer, send_buffer.Length, remoteep);
            ok = true;
        } catch (Exception send_exception) {
            Console.WriteLine(" Multicast send failed...  " + send_exception.Message);
        }

        return ok;
    }

    public static string BuildAuthString(string clientID) {
        string authhash = getHashSha256("A2s%weq!23AQ00nf[|" + getMyIp() + "|" + clientID);
        return authhash;
    }

    public static string getHashSha256(string text) {
        byte[] bytes = Encoding.UTF8.GetBytes(text);
        SHA256Managed hashstring = new SHA256Managed();
        byte[] hash = hashstring.ComputeHash(bytes);
        string hashString = string.Empty;
        foreach (byte x in hash) {
            hashString += String.Format("{0:x2}", x);
        }
        return hashString;
    }

    public static string getMyIp() {
        if (myIPAddress == null) {
            string hostName = Dns.GetHostName();
            foreach (IPAddress addr in Dns.GetHostEntry(hostName).AddressList) {
                if (addr.ToString().Contains("192.168.200.")) {
                    myIPAddress = addr.ToString();
                    return myIPAddress;
                }
            }
            return null;
        } else {
            return myIPAddress;
        }
    }

    public static string GetMac() {
        NetworkInterface _nic = NetworkInterface.GetAllNetworkInterfaces().First();
        string mac = _nic.GetPhysicalAddress().ToString();

        foreach (NetworkInterface n in NetworkInterface.GetAllNetworkInterfaces()) {
            if (n.NetworkInterfaceType == NetworkInterfaceType.Ethernet) {
                mac = n.GetPhysicalAddress().ToString();
                break;
            }
        }
        return mac[0].ToString() + mac[1].ToString() + "-" + mac[2].ToString() + mac[3].ToString() + "-" + mac[4].ToString() + mac[5].ToString() +
            "-" + mac[6].ToString() + mac[7].ToString() + "-" + mac[8].ToString() + mac[9].ToString() + "-" + mac[10].ToString() + mac[11].ToString();
    }

    public static void CloseAllConnections() {
        Console.WriteLine("Close all connections called, attempting to kill all UDP listers and close sending socket");
        while (listeners.Count > 0) {
            listeners[listeners.Keys.First()].End();
        }
        sending_socket.Close();
        sending_socket = null;
    }

    public static UDPHelper SpawnNewListener(string ipFilter, int port, UDPDelegate del, bool isMulticast = false, string multicastIPGroup = "239.123.123.1") {
        if (listeners.ContainsKey(port)) {
            Console.WriteLine("Listener already spawned for this port, adding the delegate to pre-existing listener");
            UDPHelper listener = listeners[port];
            KeyValuePair<string, UDPDelegate> delegatepair = new KeyValuePair<string, UDPDelegate>(ipFilter, del);
            listener.parentDelegates.Add(delegatepair);
            listeners[port] = listener; //do we need to do this?
            return listener;
        } else {
            Console.WriteLine("Creating new lister for port " + port);
            UDPHelper helper = new UDPHelper();
            KeyValuePair<string, UDPDelegate> delegatepair = new KeyValuePair<string, UDPDelegate>(ipFilter, del);
            helper.parentDelegates.Add(delegatepair);
            helper.listenPort = port;
            helper.isMulticast = isMulticast;
            helper.multicastIPGroup = multicastIPGroup;
            helper.Start();
            listeners.Add(port, helper);
            return helper;
        }
    }
    #endregion
}
