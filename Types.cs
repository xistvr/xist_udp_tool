﻿namespace xist_udp_tool {
    public class Port {

        // MATCHMAKER SEND
        public static readonly int UDPGenericSendPort = 11000;
        public static readonly int UDPHandTrackerSendPort = 11103;
        public static readonly int UDPSLAMSensorSendPort = 11104;
        public static readonly int UDPAppLauncherSendPort = 11106;

        // MATCHMAKER LISTEN
        public static readonly int UDPUnityClientsPort = 11001;
        public static readonly int UDPHandTrackerPort = 11003;
        public static readonly int UDPSLAMSensorPort = 11004;
        public static readonly int UDPAppLauncherPort = 11006;

        // GAMEMANAGER Listen
        public static readonly int commandServerPort = 10006;

        // GAMEMANAGER Send
        public static readonly int commandClientPort = 10106;
    }

    public enum DeviceType {
        CLIENT,
        HAND,
        SLAM,
        LAUNCHER,
        ANDROID_LAUNCHER
    }
}
